(ns client.views
  (:require
   [re-posh.core :refer [subscribe dispatch]]
   [client.events :as events]
   [client.subs :as subs]))

(defn render-todo-form [form]
  (let [{id :db/id
         title :create-todo-form/title} form]
    [:div
     [:input
      {:type "text"
       :value title
       :on-change #(dispatch [::events/set-todo-form-title id (-> % .-target .-value)])}]
     [:button
      {:on-click #(dispatch [::events/create-todo id title])}
      "Create"]]))

(defn todo-form []
  (let [form (subscribe [::subs/create-todo-form])]
    (fn []
      (render-todo-form @form))))

(defn render-task-list-item [task]
  (let [{id :db/id
         title :task/title
         done? :task/done?} task]
    [:div
     [:input {:type "checkbox"
              :on-change #(dispatch [::events/set-task-status id (not done?)])}]
     [:span title]]))

(defn task-list-item [id]
  (let [task (subscribe [::subs/task id])]
    (fn []
      (render-task-list-item @task))))

(defn task-list []
  (let [task-ids (subscribe [::subs/task-ids])]
    (fn []
      [:div
       (for [task-id @task-ids]
         ^{:key task-id} [task-list-item task-id])])))

(defn main-panel []
  [:div
   [:h1 "TodoMVC"]
   [todo-form]
   [task-list]])