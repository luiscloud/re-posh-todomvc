(ns client.db
  (:require [datascript.core :as datascript]
            [re-posh.core :as re-posh]))

(def initial-db
  [{:db/id                        -1
    :data/type                     :create-todo-form
    :create-todo-form/title        ""}
   {:db/id -2
    :data/type :task
    :task/title "Learn Clojure a little bit"
    :task/done? false}
   {:db/id -3
    :data/type :task
    :task/title "Have a coffee"
    :task/done? false}])

(def conn (datascript/create-conn))

(re-posh/connect! conn)