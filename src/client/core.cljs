(ns client.core
  (:require
   [reagent.dom :as reagent]
   [re-posh.core :as re-posh]
   [client.views :as views]
   [client.events :as events]))

(defn mount []
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export ^:dev/after-load -main []
  (re-posh/dispatch-sync [::events/initialize-db])
  (mount))